<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'firstname' => $faker->unique()->firstname,
        'lastname' => $faker->unique()->lastname,
        'address' => $faker->address,
        'email' => $faker->unique()->email,
    ];
});
